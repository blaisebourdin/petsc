#!/usr/bin/env python
import config.base
import config.package
import os

class Configure(config.package.Package):
  def __init__(self, framework):
    config.package.Package.__init__(self, framework)
    self.gitcommit  = 'v2021-01-20'
    self.download   = ['https://github.com/gsjaardema/seacas/archive/'+self.gitcommit+'.tar.gz',]
    self.downloadfilename = 'seacas-'+self.gitcommit[1:]

    self.functions  = ['ex_close']
    self.includes   = ['exodusII.h']
    self.liblist    = [['libexodus.a'], ]
    self.altlibdir  = '.'
    return

  def setupDependencies(self, framework):
    config.package.Package.setupDependencies(self, framework)
    self.compilerFlags   = framework.require('config.compilerFlags', self)
    self.netcdf          = framework.require('config.packages.netcdf', self)
    self.cmake           = framework.require('PETSc.packages.cmake', self)
    self.sharedLibraries = framework.require('PETSc.utilities.sharedLibraries', self)
    self.deps            = [self.netcdf,self.cmake]
    return

  def Install(self):
    import os
    import os.path
    self.logPrintBox('Configuring ExodusII; this may take several minutes')
    args = ['-DCMAKE_INSTALL_PREFIX='+self.installDir,]
    args.append('-DCMAKE_VERBOSE_MAKEFILE=1')

    args.append('-DACCESSDIR:PATH='+self.installDir)
    args.append('-DCMAKE_INSTALL_RPATH:PATH='+os.path.join(self.installDir,'lib'))
    args.append('-DHDF5_DIR:PATH='+self.installDir)
    args.append('-DCMAKE_RANLIB='+self.setCompilers.RANLIB)
    args.append('-DCMAKE_AR='+self.setCompilers.AR)

    self.setCompilers.pushLanguage('C')
    args.append('-DCMAKE_C_COMPILER:FILEPATH="'+self.setCompilers.getCompiler()+'"')
    self.setCompilers.popLanguage()

    self.setCompilers.pushLanguage('C++')
    args.append('-DCMAKE_CXX_COMPILER:FILEPATH="'+self.setCompilers.getCompiler()+'"')
    cxxflags = self.setCompilers.getCompilerFlags()
    args.append('-DCMAKE_CXX_FLAGS:STRING="'+cxxflags+'"')
    args.append('-DCMAKE_CXX_FLAGS_DEBUG:STRING="'+cxxflags+'"')
    args.append('-DCMAKE_CXX_FLAGS_RELEASE:STRING="'+cxxflags+'"')
    self.setCompilers.popLanguage()

    # building the fortran library is technically not required to add exodus support
    # we build it anyway so that fortran users can still use exodus functions directly 
    # from their code
    if hasattr(self.setCompilers, 'FC'):
      self.setCompilers.pushLanguage('FC')
      args.append('-DCMAKE_Fortran_COMPILER:FILEPATH="'+self.setCompilers.getCompiler()+'"')
      fflags = self.setCompilers.getCompilerFlags()
      args.append('-DCMAKE_Fortran_FLAGS:STRING="'+fflags+'"')
      args.append('-DCMAKE_Fortran_FLAGS_DEBUG:STRING="'+fflags+'"')
      args.append('-DCMAKE_Fortran_FLAGS_RELEASE:STRING="'+fflags+'"')
      args.append('-DSEACASProj_ENABLE_SEACASExodus_for=ON')
      args.append('-DSEACASProj_ENABLE_SEACASExoIIv2for32=ON')
      args.append('-DSEACASExodus_for_ENABLE_EXAMPLES:BOOL=OFF')
      args.append('-DSEACASExodus_for_ENABLE_TESTS:BOOL=OFF')
      self.liblist = [['libexoIIv2for32.a'] + libs for libs in self.liblist] + self.liblist
      self.setCompilers.popLanguage()
    else:
      args.append('-DSEACASProj_ENABLE_SEACASExodus_for=OFF')
      args.append('-DSEACASProj_ENABLE_SEACASExoIIv2for32=OFF')

    args.append('-DSEACASProj_ENABLE_SEACASExodus=ON')
    args.append('-DSEACASProj_ENABLE_TESTS=OFF')
    args.append('-DSEACASProj_SKIP_FORTRANCINTERFACE_VERIFY_TEST:BOOL=ON')
    args.append('-DTPL_ENABLE_Matio:BOOL=OFF')
    args.append('-DTPL_ENABLE_Netcdf:BOOL=ON')
    args.append('-DTPL_ENABLE_MPI=OFF')
    args.append('-DTPL_ENABLE_Pamgen=OFF')
    args.append('-DTPL_ENABLE_CGNS:BOOL=OFF')
    if not self.netcdf.directory:
      raise RuntimeError('NetCDF dir is not known! ExodusII requires explicit path to NetCDF. Suggest using --with-netcdf-dir or --download-netcdf')
    else:
      args.append('-DNetCDF_DIR:PATH='+self.netcdf.directory)
    if self.sharedLibraries.useShared:
      args.append('-DBUILD_SHARED_LIBS:BOOL=ON')
      args.append('-DSEACASExodus_ENABLE_SHARED:BOOL=ON')
    if self.compilerFlags.debugging:
      args.append('-DCMAKE_BUILD_TYPE=Debug')
    else:
      args.append('-DCMAKE_BUILD_TYPE=Release')
    args.append('-DSEACASExodus_ENABLE_TESTS:BOOL=OFF')
    args.append('-DSEACASProj_ENABLE_EXAMPLES:BOOL=OFF')
    args.append('-DSEACASProj_ENABLE_TESTS:BOOL=OFF')

    self.logPrintBox('Compiling ExodusII; this may take several minutes')
    try:
      builddir = os.path.join(self.packageDir,'petsc-build')
      if os.path.exists(builddir):
        import shutil
        shutil.rmtree(builddir)
      os.makedirs(builddir)
      output,err,ret = config.base.Configure.executeShellCommand('cd {0} && cmake {1} .. && make && make install'.format(builddir,' '.join(args)), timeout=2500, log = self.framework.log)
    except RuntimeError, e:
      raise RuntimeError('Error running make on exodusII: '+str(e))
    return self.installDir
