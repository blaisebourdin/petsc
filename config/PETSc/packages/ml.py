#!/usr/bin/env python
import config.base
import config.package
import os

class Configure(config.package.Package):
  def __init__(self, framework):
    config.package.Package.__init__(self, framework)
    self.version           = '13.2.0'
    self.gitcommit         = 'v{0}'.format(self.version)
    self.versionname       = 'PACKAGE_VERSION'
    self.download          = ['https://gitlab.com/petsc/pkg-trilinos-ml/-/archive/{0}/pkg-trilinos-ml-{0}.tar.gz'.format(self.gitcommit)]
    self.downloadfilename  = 'pkg-trilinos-ml-{0}'.format(self.gitcommit)
    self.functions         = ['ML_Set_PrintLevel']
    self.includes          = ['ml_include.h']
    self.liblist           = [['libml.a']]
    self.altlibdir  = '.'
    self.license           = 'https://trilinos.github.io'
    self.buildLanguages    = ['Cxx']
    self.precisions        = ['double']
    self.complex           = 0
    self.downloadonWindows = 1
    self.requires32bitint  = 1;  # ml uses a combination of "global" indices that can be 64 bit and local indices that are always int therefore it is
                                 # essentially impossible to use ML's 64 bit integer mode with PETSc's --with-64-bit-indices
                                 
  def setupDependencies(self, framework):
    config.package.Package.setupDependencies(self, framework)
    self.compilerFlags   = framework.require('config.compilerFlags', self)
    self.mpi        = framework.require('config.packages.MPI',self)
    self.blasLapack = framework.require('config.packages.BlasLapack',self)
    self.cmake           = framework.require('PETSc.packages.cmake', self)
    self.sharedLibraries = framework.require('PETSc.utilities.sharedLibraries', self)
    self.deps            = [self.mpi,self.blasLapack,self.cmake]
    return

  def generateLibList(self,dir):
    import os
    '''Normally the one in package.py is used, but ML requires the extra C++ library'''
    libs = ['libml']
    alllibs = []
    for l in libs:
      alllibs.append(l+'.a')
    # Now specify -L ml-lib-path only to the first library
    alllibs[0] = os.path.join(dir,alllibs[0])
    import config.setCompilers
    #if self.languages.clanguage == 'C':
    alllibs.extend(self.compilers.cxxlibs)
    return [alllibs]

  def Install(self):
    import os
    import os.path
    self.logPrintBox('Configuring ML; this may take several minutes')
    args = ['-DCMAKE_INSTALL_PREFIX='+self.installDir,]
    args.append('-DCMAKE_VERBOSE_MAKEFILE=1')

    args.append('-DTrilinos_ENABLE_ALL_OPTIONAL_PACKAGES=OFF')
    args.append('-DTrilinos_ENABLE_ALL_PACKAGES=OFF')
    args.append('-DTrilinos_ENABLE_ML=ON')
    args.append('-DTPL_BLAS_LIBRARIES="'+self.libraries.toString(self.blasLapack.dlib)+'"')
    args.append('-DTPL_LAPACK_LIBRARIES="'+self.libraries.toString(self.blasLapack.dlib)+'"')
    args.append('-DBUILD_SHARED_LIBS=ON')
    args.append('-DTPL_ENABLE_MPI=ON')
    if not hasattr(self.compilers, 'FC'):
      args.append('-DTrilinos_ENABLE_Fortran=OFF')

    if self.compilerFlags.debugging:
      args.append('-DCMAKE_BUILD_TYPE=Debug')
    else:
      args.append('-DCMAKE_BUILD_TYPE=Release')

    try:
      builddir = os.path.join(self.packageDir,'petsc-build')
      if os.path.exists(builddir):
        import shutil
        shutil.rmtree(builddir)
      os.makedirs(builddir)
      output,err,ret = config.base.Configure.executeShellCommand("cd {0} && cmake {1} .. && make && make install".format(builddir,' '.join(args)), timeout=2500, log = self.framework.log)
    except RuntimeError, e:
      raise RuntimeError('Error configuring ML: '+str(e))
    return self.installDir
